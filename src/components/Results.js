import React from "react";

const Results = ({ data }) => {
  const showData = () => {
    if (data) {
      if (data.file === "No file") {
        return <p>{data.type}</p>;
      } else if (data.file === "weather.dat") {
        return (
          <>
            <p>Day with lowest temperature range</p>
            <p>Day: {data.day}</p>
            <p>Temperature range: {data.tempRange}</p>
          </>
        );
      } else {
        return (
          <>
            <p>Premier League team with lowest goal difference</p>
            <p>Team: {data.team}</p>
            <p>Goal difference: {data.goalDiff}</p>
          </>
        );
      }
    } else return null;
  };

  return <div className="results">{data && showData()}</div>;
};

export default Results;
