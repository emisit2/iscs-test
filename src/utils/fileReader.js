export const openFile = (e, setFile, setData) => {
  const validExtension = "dat";
  const file = e.target.files[0];
  const fileName = e.target.files[0].name;
  const fileExt = fileName.substring(fileName.indexOf(".") + 1);
  let error = null;
  if (!file || !fileName) {
    error = { file: "No file", type: "The file has not been uploded correctly" };
  } else if (fileExt !== validExtension) {
    error = { file: "No file", type: "The file extention cannot be read" };
  }
  if (error) {
    setData(error);
  } else {
    const reader = new FileReader();
    reader.readAsBinaryString(e.target.files[0]);
    reader.onload = () => {
      const file = reader.result.split("\n").map((el) => {
        return el.split(" ").filter((el) => el !== "");
      });
      setFile({ name: fileName, data: file });
    };
  }
};

export const readWeatherData = (file, setData) => {
  if (file) {
    const dataParsed = file.data.filter((el) => el.length > 13);
    const dataFiltered = dataParsed.map((el) => el.filter((_e, i) => i < 3));
    const dataToNum = dataFiltered.map((el) => el.map((e) => +e.replace("*", "")));
    const sortedArrays = dataToNum.map((el) => {
      return { day: el[0], tempRange: el.filter((_e, i) => i > 0).reduce((a, b) => +a - +b) };
    });
    const dayMinTempRange = sortedArrays.sort((a, b) => a.tempRange - b.tempRange)[1];
    setData({ file: "weather.dat", ...dayMinTempRange });
  }
};

export const readFootballData = (file, setData) => {
  if (file) {
    const dataParsed = file.data.filter((el) => el.length === 10);
    const goalsFiltered = dataParsed.map((el) => el.filter((_e, i) => i === 1 || i === 6 || i === 8));
    const sortedArrays = goalsFiltered.map((el) => {
      return {
        team: el[0].replace("_", " "),
        goalDiff: el.filter((_e, i) => i > 0).reduce((a, b) => +a - +b),
      };
    });
    const getAbsoluteDifference = sortedArrays.map((el) => {
      return el.goalDiff < 0 ? { ...el, goalDiff: el.goalDiff + -el.goalDiff * 2 } : el;
    });
    const teamLowestGoalDifference = getAbsoluteDifference.sort((a, b) => a.goalDiff - b.goalDiff)[0];
    const getBackInitalDifferenceValue = sortedArrays.find((el) => el.team === teamLowestGoalDifference.team);
    setData({ file: "football.dat", ...getBackInitalDifferenceValue });
  }
};
