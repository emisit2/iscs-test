# Test per ISCS

Questo progetto è stato creato con [Create React App](https://github.com/facebook/create-react-app).

L'applicazione consente di estrarre ed elaborare dei dati dai file weather.dat e football.dat, mostrando in ultimo i risultati secondo le indicazioni della consegna.

I file si trovano nella cartella testFiles.

L'applicazione consente di estrapolare le informazioni richieste (escursione minima e differenza reti minima) attraverso un solo input.

In base al nome del file verrà infatti elaborato il risultato relativo a uno dei due file.
L'applicazione verifica inoltre il corretto upload e l'estensione dei file, che deve necessariamente corrispondere al tipo ".dat", in caso contrario sarà mostrato un errore.

### Yarn

Per poter utilizzare l'applicazione occorre inizialmente procedere all'installazione dei Node modules attraverso il comando Yarn.

### Yarn start

Una volta installate le dependencies necessarie è possibile avviare l'applicazione tramite il comando yarn start. L'applicazione dovrebbe dunque avviarsi.
