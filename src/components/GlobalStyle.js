import React from "react";

const GlobalStyle = ({ children }) => <div className="app">{children}</div>;
export default GlobalStyle;
