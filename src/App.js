import React, { useState, useEffect } from "react";
import { readWeatherData, readFootballData } from "./utils/fileReader";
import GlobalStyle from "./components/GlobalStyle";
import Form from "./components/Form";
import Results from "./components/Results";

const App = () => {
  const [data, setData] = useState();
  const [file, setFile] = useState();

  useEffect(() => {
    if (file && file.name === "weather.dat") {
      readWeatherData(file, setData);
    } else readFootballData(file, setData);
  }, [file]);

  return (
    <GlobalStyle>
      <Results data={data} />
      <Form setFile={setFile} setData={setData} />
    </GlobalStyle>
  );
};

export default App;
