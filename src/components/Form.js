import React from "react";
import { openFile } from "../utils/fileReader";

const Form = ({ setFile, setData }) => {
  return (
    <div className="form">
      <label className="label">
        Click here to open the file
        <input type="file" accept="*.dat" onChange={(e) => openFile(e, setFile, setData)} className="input" />
      </label>
    </div>
  );
};

export default Form;
